# Crayonic B. V. Maven Repository

Used for delivery of compiled libraries 



### Usage in Android Studio Gradle
Add repository into your main application build script. 

```gradle

allprojects {

    repositories {
        maven { url "https://gitlab.com/crayonic/maven/raw/master/maven/" }  // use this for device_sdk* libraries
    }
    
}
```

### Attach library to module
Add library to dependencies in your module build.gradle. 

```gradle
dependencies {
    compile '$domain.$group:$artifact:$version'
}
```

----

## Latest versions

com.crayonic.pensdk:1.0.1 [tree](https://gitlab.com/lukassos/artifacts/tree/master/maven/com/crayonic/pensdk/1.0.1)
com.crayonic.sigma:1.40.1 [tree](https://gitlab.com/lukassos/artifacts/tree/master/maven/com/crayonic/sigma/1.40.1)

com.crayonic:device_sdk:0.0.4 [tree](https://gitlab.com/lukassos/artifacts/tree/master/maven/com/crayonic/device_sdk/0.0.4)
com.crayonic:device_sdk_android:0.0.4 [tree](https://gitlab.com/lukassos/artifacts/tree/master/maven/com/crayonic/device_sdk_android/0.0.4)
